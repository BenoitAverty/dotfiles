if ! pacman -Qs nodenv > /dev/null; then
    pamac build --no-confirm nodenv
fi

if ! pacman -Qs nodenv-node-build > /dev/null; then
    pamac build --no-confirm nodenv-node-build
fi

git clone https://github.com/ouchxp/nodenv-nvmrc.git $(nodenv root)/plugins/nodenv-nvmrc
git clone https://github.com/nodenv/nodenv-package-json-engine.git $(nodenv root)/plugins/nodenv-package-json-engine

git clone https://github.com/lukechilds/zsh-better-npm-completion.git ~/.zsh-better-npm-completion
(cd ~/.zsh-better-npm-completion && git pull)

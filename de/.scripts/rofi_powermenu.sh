#!/bin/bash

choice=$(echo -e "Cancel\nLock\nExit\nShutdown\nReboot\nSuspend to Ram\nSuspend to Disk\nHybrid sleep" | rofi -dmenu -p "Power: " -i)

case "$choice" in
  "Cancel")
    :
    ;;
  "Lock")
    light-locker-command -l
    ;;
  "Exit")
    i3-msg exit;
    ;;
  "Shutdown")
    systemctl poweroff;
    ;;
  "Reboot")
    systemctl reboot;
    ;;
  "Suspend to Ram")
    systemctl suspend;
    ;;
  "Suspend to Disk")
    systemctl hibernate;
    ;;
  "Hybrid sleep")
    systemctl hybrid-sleep;
    ;;
esac

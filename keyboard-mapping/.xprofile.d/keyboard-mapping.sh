# load custom keymap
# CAREFUL you should comment this and test w<hen modifying the keymap because it can break keyboard.
# If this is commented then at least rebooting keeps default
test -f ~/.Xkeymap && xkbcomp ~/.Xkeymap $DISPLAY

#!/bin/bash

skip_script=false
# Check if --no-script argument is provided
if [ "$1" == "--no-script" ]; then
    skip_script=true
    shift # Remove the argument from the list
fi

# Check if package name is provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 [--no-script] <package_name>"
    exit 1
fi

# Extract package name
package_name=$1

# Print message
echo "Installing package $package_name"

# Run the package specific script if not skipped
if [ "$skip_script" = false ]; then
    if [ -f "./$package_name.setup.sh" ]; then
        echo "Setting up..."
        sh -c "./$package_name.setup.sh"
    else
        echo "Warn: The script ./$package_name.setup.sh does not exist."
    fi
else
    echo "Skipping setup script execution as per --no-script argument."
fi

# Run the stow command
if [ -d "./${package_name}" ]; then
    echo "Installing files..."
    stow -vv --no-folding -S "$package_name" --ignore .gitkeep --ignore .skip
else
    echo "Warn: The directory ./$package_name does not exist."
fi

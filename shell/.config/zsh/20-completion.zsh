# Additional completions from zsh-completions
fpath=(/usr/share/zsh/site-functions/ $fpath)

autoload -U compinit; compinit
_comp_options+=(globdots) # allow dotfiles in file completion

# define completers
zstyle ':completion:*' completer _extensions _complete

# cache completions (for thinhs like apt, ...)
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "/home/benoit/.cache/zsh/.zcompcache"

# Group matches and Describe
zstyle ':completion:*:matches' group 'yes'
zstyle ':completion:*:options' description 'yes'
zstyle ':completion:*:options' auto-description '%d'
zstyle ':completion:*:descriptions' format $'\e[01;33m -- %d --\e[0m'
zstyle ':completion:*:messages' format $'\e[01;35m -- %d --\e[0m'
zstyle ':completion:*:warnings' format $'\e[01;31m -- No Matches Found --\e[0m'
zstyle ':completion:*' group-name ''

# autosuggestion
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

setopt MENU_COMPLETE        # Automatically highlight first element of completion menu
setopt AUTO_LIST            # Automatically list choices on ambiguous completion.
setopt COMPLETE_IN_WORD     # Complete from both ends of a word.
setopt LIST_PACKED          # options take less space

zstyle ':completion:*' insert-tab false # tab on empty line opens menu
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*' # Case insensitive tab completion, and complete partial words
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"         # Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' rehash true                              # automatically find new executables in path
zstyle ':completion:*' menu select                             # Use completion menu
zstyle ':completion:*' file-sort access

#substring history search (instead of only prefix) -> overriden by fzf
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
zmodload zsh/terminfo
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# Completion keybindings
zmodload zsh/complist
bindkey -M menuselect '^M' .accept-line # enter automatically sends the command. Continue typing to fill the rest of command or use esc
bindkey -M menuselect '^H' send-break # backspace leaves the menu, erasing the completed part
bindkey -M menuselect '^[' accept-line # escape leaves the menu but keeps the selected entry
bindkey -M menuselect '\t' accept-and-infer-next-history # Tab accepts the selection but keeps the menu open. Especially useful while cd-ing in deep folders
bindkey -M menuselect '^F' history-incremental-search-forward # search fuzzy in the menu
bindkey -M menuselect '^R' history-incremental-search-forward # search fuzzy (backwards) in the menu
bindkey -M menuselect '^[i' vi-insert # alt+i enters interactive mode


# commands : man zshmodules - Search for “THE ZSH/COMPLIST MODULE”

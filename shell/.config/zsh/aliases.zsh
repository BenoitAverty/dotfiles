# ZSH plugin that reminds about aliases
source /usr/share/zsh/plugins/zsh-you-should-use/you-should-use.plugin.zsh

alias l='ls'
alias ls="ls --color=auto"
alias ll="ls -lgh"
alias lla="ll -a"

alias cp="cp -i"
alias df='df -h'       # Human-readable sizes
alias free='free -m'   # Show sizes in MB

alias cat="bat"

alias systemctl="sudo systemctl"

alias p="popd"

#pacman / pamac
alias parem="sudo pacman -Rs" # remove a package
alias pas="pamac search " # search for a package (including aur)
alias pain="sudo pacman -S" #install a package
alias pab="pamac build" # build a package from aur
alias pafiles="pacman -Ql" #list files of a package
alias paup="sudo pacman -Syu" # system upgrade
alias paown="pacman -Qo" # find package that owns a file

# Edit files in IntelliJ in light edit mode
idea() {
    command idea -e "$@" > /dev/null 2>&1
}

mkcd() {
    mkdir "$@" && cd "$_"
}

# help me get used to procs
orig_ps=$(which ps)
ps() {
  if [ -t 1 ]; then
    echo -e "\033[1;33mthink of procs !\033[0m";
  fi
  $orig_ps "$@"
  if [ -t 1 ]; then
    echo -e "\033[1;33mthink of procs !\033[0m";
  fi
}

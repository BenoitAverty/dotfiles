# Set up fzf key bindings and fuzzy completion
# Ctrl + R will search in history with fzf
# Ctrl + T will search for a file and insert in command line
# Alt + C will search for a file and cd to it
source <(fzf --zsh)

fzf-and-run-widget() {
  fzf-history-widget
  zle accept-line
}
zle     -N   fzf-and-run-widget
#bindkey '^R' fzf-and-run-widget
#bindkey "$terminfo[kcuu1]" fzf-and-run-widget
#bindkey "$terminfo[kcud1]" fzf-and-run-widget
#bindkey '^[[A' fzf-and-run-widget
#bindkey '^[[B' fzf-and-run-widget

# help me get used to it
orig_find=$(which find)
find() {
  if [ -t 1 ]; then
    echo -e "\033[1;33mthink of fzf !\033[0m";
  fi
  $orig_find "$@"
  if [ -t 1 ]; then
    echo -e "\033[1;33mthink of fzf !\033[0m";
  fi
}

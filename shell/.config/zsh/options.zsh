# Save the history
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory

# General options
setopt extendedglob            # Extended globbing. Allows using regular expressions with *
setopt nocaseglob              # Case insensitive globbing
setopt rcexpandparam           # Array expension with parameters
setopt numericglobsort         # Sort filenames numerically when it makes sense
setopt nobeep                  # No beep
setopt appendhistory           # Immediately append history instead of overwriting
setopt histignorealldups       # If a new command is a duplicate, remove the older one
setopt autocd                  # if only directory path is entered, cd there.
setopt inc_append_history      # save commands are added to the history immediately, otherwise only when shell exits.
setopt histignorespace         # Don't save commands that start with space
setopt auto_pushd           # Push the current directory visited on the stack.
setopt pushd_ignore_dups    # Do not store duplicates in the stack.
setopt pushd_silent         # Do not print the directory stack after pushd or popd.

autoload -U colors; colors
# Syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Dynamic title
autoload -Uz add-zsh-hook

function xterm_title_precmd () {
	print -Pn -- '\e]2;%n@%m %~\a'
	[[ "$TERM" == 'screen'* ]] && print -Pn -- '\e_\005{g}%n\005{-}@\005{m}%m\005{-} \005{B}%~\005{-}\e\\'
}

function xterm_title_preexec () {
	print -Pn -- '\e]2;%n@%m %~ %# ' && print -n -- "${(q)1}\a"
	[[ "$TERM" == 'screen'* ]] && { print -Pn -- '\e_\005{g}%n\005{-}@\005{m}%m\005{-} \005{B}%~\005{-} %# ' && print -n -- "${(q)1}\e\\"; }
}

if [[ "$TERM" == (Eterm*|alacritty*|aterm*|foot*|gnome*|konsole*|kterm*|putty*|rxvt*|screen*|wezterm*|tmux*|xterm*) ]]; then
	add-zsh-hook -Uz precmd xterm_title_precmd
	add-zsh-hook -Uz preexec xterm_title_preexec
fi

# Dots expanding
fpath=(~/.local/share/zsh/manydots-magic $fpath)
autoload -Uz manydots-magic
manydots-magic

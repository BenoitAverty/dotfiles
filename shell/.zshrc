# execute zsh files in a config folder for modularity for *.zsh files in .config/zsh directory
for file in ~/.config/zsh/*.zsh; do
    if [ -f "$file" ]; then
        . $file
    fi
done

# starship
eval "$(starship init zsh)"

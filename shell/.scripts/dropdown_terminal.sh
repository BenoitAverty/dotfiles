#!/bin/bash

_term() {
  echo "Caught SIGTERM signal!"
  kill -TERM "$child" 2>/dev/null
  exit 1
}

trap _term SIGTERM

alacritty --title dropdown_terminal --class dropdown_terminal &

child=$!
wait "$child"

echo $0
$0 &

sudo pacman -S --needed alacritty zsh starship zsh-history-substring-search zsh-syntax-highlighting zsh-completions zsh-autosuggestions fzf bat broot procs

# Install zoxide. The version in arch repos is not up to date and has a bug with cd completion
curl -sSfL https://raw.githubusercontent.com/ajeetdsouza/zoxide/main/install.sh | sh

# zsh-autocomplete
if ! pacman -Qs zsh-autocomplete-git > /dev/null; then
    sudo pamac build --no-confirm zsh-autocomplete-git
fi

# zsh-autocomplete
if ! pacman -Qs zsh-you-should-use > /dev/null; then
    sudo pamac build --no-confirm zsh-you-should-use
fi

if [ "$SHELL" != "/usr/bin/zsh" ]; then
    chsh -s /usr/bin/zsh
fi

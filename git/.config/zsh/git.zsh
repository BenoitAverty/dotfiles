# Outputs the name of the current branch
# Usage example: git pull origin $(git_current_branch)
# Using '--quiet' with 'symbolic-ref' will not cause a fatal error (128) if
# it's not a symbolic ref, but in a Git repo.
function git_current_branch() {
  local ref
  ref=$(git symbolic-ref --quiet HEAD 2> /dev/null)
  local ret=$?
  if [[ $ret != 0 ]]; then
    [[ $ret == 128 ]] && return  # no git repo.
    ref=$(git rev-parse --short HEAD 2> /dev/null) || return
  fi
  echo ${ref#refs/heads/}
}

alias g='git'
alias gsh='git stash'
alias gstp='git stash pop'
alias gst='git status'
alias gc='git commit'
alias gcamnoe='git commit --amend --no-edit'
alias gca='git commit -a'
alias ga='git add'
alias gs='git switch'
alias gsc='git switch -c'
alias gr='git restore'
alias ggpush='git push origin "$(git_current_branch)"'
alias gd='git diff'
alias grbc='git rebase --continue'
alias gl="git pull"
alias gf="git fetch"
alias gb="git branch"

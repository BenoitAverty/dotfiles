# My dotfiles

## Prerequisites

All of these have been made on a fresh Manjaro i3 installation.

There may be missing files or packages if trying to use these dotfiles on other platforms (also, installation of required packages may be harder on a non-Archlinux distro)

## Install

the dotfiles are separated in "packages" (shell, de, git...)

they are installed using the `install-package.sh` script. (example : `install-package.sh shell` will configure zsh, the prompt...)

Dependencies between packages are not documented but init-profile is probably needed for several other packages.

## Additional steps on Dell Latitude 7650

* Bluetooth card only supported from linux 6.6
